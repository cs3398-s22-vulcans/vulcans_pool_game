# Project Name - Vulcan Online Pool Game
- Created by Ryan Larsen, Colton Tyran, Alex Walker, Eastland Crane, Gabriel Simon
- An online pool game that can be played by two people or solo for practice. 
- Was created for pool enthusiests!
- Doing this for a fun game and practice among software engineering practice with a team.

​
> Live demo [_here_](https://vulcanswebsite.ecrane0107.repl.co/). <!-- If you have the project hosted somewhere, include the link here. -->
​
## Table of Contents
* [Description](#vision questions)
* [General Info](#general-information)
* [Technologies Used](#technologies-used)
* [Features](#features)
* [Screenshots](#screenshots)
* [Setup](#setup)
* [Usage](#usage)
* [Project Status](#project-status)
* [Room for Improvement](#room-for-improvement)
* [Acknowledgements](#acknowledgements)
* [Contact](#contact)
* [License](#license)
​
​
​
## General Information
- A website that contains a pool game intended to be played by an user against a bot or solo for practice.
- It intends for the user to have fun while playing a strategic/skillful game.
- What is the purpose of your project?
- Why did you undertake it?
​
​
​
## Technologies Used
- Jira/Bitbucket/Discord/Slack for planning and team communication.

- GitKraken for source control and team remote repository.

- https://github.com/max-kov/pool for the pool game repository we are modifying.

- Numpy for various mathematical formula and functions in the Python Langauge.

- https://pypi.org/project/zope.event for an event pulishing API.

- PyUnit to run tests on our progress builds.

- repl.it for creating a website to host the based application
​
## Features
List the ready features here:
-
- I, Ryan, as a casual pool player, want a predictive line of where the ball will be launched so that it is easier and more user-friendly to aim the shot.

- I, Colton Tyran, as a student of Texas State University, I want to create a physics based pool game so that I can understand realistic games are made behind the scenes for experience.

- I, Gabriel Simon, as one of the creators behind this project, want to use a 2D physics engine to create an authentic pool experience that can be customized for fun game modes.
​
- I, Alex, as a pool player, would like to have accurate collision detection for the balls and walls. 
https://bitbucket.org/cs3398-s22-vulcans/vulcans_pool_game/src/master/pool/
​
## Screenshots
​![Pool-Image](https://i.gyazo.com/d3f6c7dbac20ae6bc11d5a42ef405eb6.png)
​
​
​
## Setup
​
​
​
## Usage
Click on the link below to get started! Have fun!
https://vulcanpoolgame.net (dead link, will update soon)
​
## Project Status
Project is: *in progress*

## Sprint 1 Reports
Ryan: added configuration for CircleCI, learned how to use and documented findings about ​PyUnit, added PyUnit configuration to CircleCI, added code fro max-kov's repository, fixed bugs in the test cases, and updated CircleCI config to run the test cases.

https://bitbucket.org/cs3398-s22-vulcans/vulcans_pool_game/commits/e66eb37fefb729d08d9052ea1c917630c3560677
https://bitbucket.org/cs3398-s22-vulcans/vulcans_pool_game/commits/be617e1b5701100a475b81df6fdf353751533d8c
https://bitbucket.org/cs3398-s22-vulcans/vulcans_pool_game/commits/54277cb92f76022e4fdafd9ab96925415d9471e8
https://bitbucket.org/cs3398-s22-vulcans/vulcans_pool_game/commits/44e1c8ff9f406d3aff69f219e2d4ae27ebcc5fbc
https://bitbucket.org/cs3398-s22-vulcans/vulcans_pool_game/commits/a77031748954428fb5fa107e22189e7713be1b86
https://bitbucket.org/cs3398-s22-vulcans/vulcans_pool_game/commits/9741f377d50f2d46fa6413d2e1996b4fab6da46b
​
## Room for Improvement
Project is currently in the process of being transferred to an online website.

Project is awaiting improvements in graphics to bring it to our standards, physics tweaks to enhance realism, 
options for ease of replayability, options for additional game modes, and various other quality of life improvements. 
​
​
## Acknowledgements
This project was inspired by Ryan Larsen, Colton Tyran, Alex Walker, Eastland Crane, Gabriel Simon gaming needs.
This project was based on online pool game, with credit to Max Kovalovs for the repository containing the pool game.
Many thanks to Dr. Lehr for the opportunity to create this project.
​
​
## Contact
Created by Ryan Larsen(rsl57@txstate.edu), Colton Tyran(ctt32@txstate.edu), Alex Walker(a_w366@txstate.edu), Eastland Crane(e_c350@txstate.edu), Gabriel Simon(gms139@txstate.edu)
​
​
<!-- Optional -->
## License
Max Kovalovs' software holds the following license, our software will follow the same rules. Including all our modifications and additions to the repository. 

MIT License

Copyright (c) 2018 Max Kovalovs

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
<!-- This project is open source and available under the [... License](). -->
​
<!-- You don't have to include all sections - just the one's relevant to your project -->