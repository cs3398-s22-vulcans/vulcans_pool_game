# Basic collision check class
# written by Alex Walker

from random import shuffle

class Collision:
	def __init__(self):
		pass

	''' 
	check each ball for collision
	input: ball to check, list of balls and pos, radius of ball
	output: 0 if no collision, ball number if hit
	'''
	@staticmethod	
	def ballCollision(ball, balls):
		collison = 0
		shuffled_balls = balls.copy()   # shuffled copy of balls to randomize order of collision for breaks
		shuffle(shuffled_balls)
		for b in shuffled_balls:
			if b.num == ball.num:
				pass				
			elif ((ball.xPos + ball.radius) >= (b.xPos - b.radius)) and ((ball.xPos - ball.radius) <= (b.xPos + b.radius)):
				if ((ball.yPos + ball.radius) >= (b.yPos - b.radius)) and ((ball.yPos - ball.radius) <= (b.yPos + b.radius)):
					collison = b.num
					break
		return collison

	'''
	check for wall collision
	input: ball, table max width (optional min), table max height (optional min)
	output: 0 if no collision, 1 if x-axis collison, 2 if y-axis collsion
	'''
	@staticmethod
	def wallCollision(ball, maxX, maxY, minX = 0, minY = 0):
		collison = 0
		if ball.xPos - ball.radius <= minX or ball.xPos + ball.radius >= maxX:
			collison += 1
		elif ball.yPos - ball.radius <= minY or ball.yPos + ball.radius >= maxY:
			collison += 2
		return collison