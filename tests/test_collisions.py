# Collision test cases
# written by Alex Walker

from pool import ball_collisions
import unittest


class CollisionTest(unittest.TestCase):

	def setUp(self):
		class Ball:
			def __init__(self, x, y, num, r):
				self.xPos = x
				self.yPos = y
				self.num = num
				self.radius = r
		self.POS1 = [(55, 40), (30, 195), (165, 15), (65, 160), (60, 160), (110, 195), (5, 30), (15, 120), (50, 50), (55, 65)]
		self.POS2 = [(50, 80), (115, 115), (170, 190), (25, 15), (70, 20), (70, 140), (65, 115), (160, 60), (95, 185), (45, 130)]
		self.RADIUS = 5
		self.balls1 = [Ball(pos[0],pos[1],num+1, self.RADIUS) for num, pos in enumerate(self.POS1)]
		self.balls2 = [Ball(pos[0],pos[1],num+1, self.RADIUS) for num, pos in enumerate(self.POS2)]
		self.table_maxX = 200
		self.table_maxY = 200

	def testBallCollision1(self):
		collision = []
		for ball in self.balls1:
			collision.append(ball_collisions.Collision.ballCollision(ball, self.balls1))
		self.assertEqual(collision, [9, 0, 0, 5, 4, 0, 0, 0, 1, 0])

	def testBallCollision2(self):
		collision = []
		for ball in self.balls2:
			collision.append(ball_collisions.Collision.ballCollision(ball, self.balls2))
		self.assertEqual(collision, [0, 0, 0, 0, 0, 0, 0, 0, 0, 0])

	def testWallCollision1(self):
		collision = []
		for ball in self.balls1:
			collision.append(ball_collisions.Collision.wallCollision(ball, self.table_maxX, self.table_maxY))
		self.assertEqual(collision, [0, 2, 0, 0, 0, 2, 1, 0, 0, 0])

	def testWallCollision2(self):
		collision = []
		for ball in self.balls2:
			collision.append(ball_collisions.Collision.wallCollision(ball, self.table_maxX, self.table_maxY))
		self.assertEqual(collision, [0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
		
if __name__ == '__main__':
	unittest.main()


















